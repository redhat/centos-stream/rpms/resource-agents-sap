#!/bin/sh

err=0

printf "\n" # readability

list_resources() {
# Verify that 3 SAP* agents are present.
  printf "Checking resource agents... "
  num="3"
  agents=$(pcs resource list ocf:heartbeat:SAP --nodesc)
  if [ "$(echo "${agents}" | wc -l)" != "${num}" ]; then
    printf "FAILED\n"
    printf "ERROR: %s matching pcs agents expected, but found:\n%s\n" "${num}" "${agents}"
    err=$((err+1))
  else
    printf "SUCCESS\n"
    printf "%s matching pcs agents found:\n%s\n" "${num}" "${agents}"
  fi
  printf "\n" # readability
}

check_bins() {
# Verify that binaries are working.
  for bin in \
    "/usr/bin/sapservices-move -h"
  do
    printf "Testing '%s'... " "${bin}"
    output=$(${bin} 2>&1)
    rc=$?
    if [ "${rc}" -gt 0 ]; then
      printf "FAILED\n"
      printf "ERROR: %s\n" "${output}"
      err=$((err+1))
    else
      printf "SUCCESS\n"
    fi
  done
  printf "\n" # readability
}

check_services() {
# Verify that the services exist. They are not loaded by design (rc=3).
  for service in \
    sapping.service \
    sappong.service
  do
    printf "Testing '%s'... " "${service}"
    output=$(systemctl status ${service} 2>&1)
    rc=$?
    if [ "${rc}" -gt 3 ]; then
      printf "FAILED\n"
      printf "ERROR: %s\n" "${output}"
      err=$((err+1))
    else
      printf "SUCCESS\n"
    fi
  done
  printf "\n" # readability
}

list_resources
check_bins
check_services

if [ "${err}" -gt 0 ]; then
  printf "ERROR: %s tests FAILED...\n" "${err}"
  exit 1
fi
